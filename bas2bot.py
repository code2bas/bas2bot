#heroku ps:scale bas2bot=1
import os
from datetime import datetime
from apscheduler.schedulers.blocking import BlockingScheduler 
from binance_connect import SIGNALS_BY_SYMBOL_TDV , SIGNALS_BY_SYMBOL , TRADE_SPORT_CURRENT
from db_connect import * 
sched = BlockingScheduler()
#----------------------------------------------------------------------------------
#  Get Config
#----------------------------------------------------------------------------------
g_time           = int(os.environ['G_TIME'])
g_max_instances  = int(os.environ['G_MAX_INSTANCES'])
#g_coins          = os.environ['G_COINS']  
g_coins          =  "BTCUSDT,BNBUSDT,DOTUSDT" #BTCUSDT,BNBUSDT,DOTUSDT" 
g_time           = 5 
g_max_instances  = 5  
print("g_time [{}] , g_max_instances [{}] , g_coin [{}]".format( g_time , g_max_instances , g_coins)) 
#================================================================================== 
#----------------------------------------------------------------------------------
#  JOB SIGNAL
#----------------------------------------------------------------------------------
#@sched.scheduled_job('cron', year='*', month='*', day='*', week='*', day_of_week='*', hour='*', minute='*', second='*/'+str(g_time))
@sched.scheduled_job('interval', seconds= g_time , max_instances=g_max_instances)
def signal_job(): 
    #print('This job signal_job.')
    #print(">>> [{}]".format(str(datetime.now())))
    coin_list = g_coins.split(",")    
    for coin in coin_list : 
        #print("*** [{},{}]".format(coin,str(datetime.now())))
        SIGNALS_BY_SYMBOL_TDV(coin)
    #print("<<< [{}]".format(str(datetime.now())))       
#================================================================================== 
       
#----------------------------------------------------------------------------------
#  JOB SIGNAL
#----------------------------------------------------------------------------------
@sched.scheduled_job('interval', seconds= 2 , max_instances=1)
def trade_job():
    TRADE_SPORT_CURRENT()
#================================================================================== 

##@sched.scheduled_job('interval', seconds= 15 , max_instances=1)
##def timed_job2():
##    print('This job is run every weekday at 15 Sec.')

##@sched.scheduled_job('interval', seconds= 30 , max_instances=1)
##def timed_job3():
##    print('This job is run every weekday at 30 Sec.')

##@sched.scheduled_job('cron', day_of_week='mon-fri', hour=17)
##def scheduled_job():
##    print('This job is run every weekday at 5pm.')

sched.start() 




#ADX           -->  ความแข็งแกร่ง   0-100
#ADX+ > ADX-   -->  uptrend
#ADX+ < ADX-   -->  downtrend
### Fillter
#ADX > EMA(ADX , 20) And ADX+ > ADX-
##Buy
#EMA(20) > EMA(20)
#-------
###Trade Trigger
##Buy    
#  EMA10 > EMA50 
#  MACD  > 0
#  RSI   < 20
#  Price > EMA60
#  ADX > EMA(ADX , 20) And ADX+ > ADX-
##Sell 
#  EMA10 < EMA50
#  MACD  < 0
#  RSI   > 80
#  Price < EMA60 
#  Cutloss > 2%-6% จาก portfolio  
#------------------------------------
#position score = cxv/EMA20
#stop loss = ขาเทุน > 2(ATR) ย้อนหลัง 20 วัน
#ATR = ค่าที่มากที่สุดของ 3 ค่า
#l ราคาสูงสุดของวันนี้ – ราคาปิดของวันก่อนหน้า l
#l ราคาปิดของวันก่อนหน้า – ราคาต่ำสุดของวันนี้ l
#l ราคาสูงสุดของวันนี้ – ราคาต่ำสุดของวันนี้ l

#%Winner ความแม่นยำ
#Payoff Ratio = Average Profit / Average Loss
#net Profit กำไร/ขาดทุน สุธิ

#c = ราคาปิด
#v = volumn
#cxv  = การซื้อขายระหว่างวัน
#Expectancy = % ความคาดหวังในการเทรด
#Expectancy = (%Average Prifit)*(%Winner) + (%Average Loss)*(%Loser)
