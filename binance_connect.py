#https://github.com/jp-developer0/Python-TradingView-TA-Library
#https://www.youtube.com/watch?v=LQQGSibtjfM
#https://developers.line.biz/media/messaging-api/emoji-list.pdf
#http://cons-robotics.com/LINEAPI/sticker.pdf
#https://unicode.org/emoji/charts-11.0/emoji-list.html
#----
from songline import Sendline
#----
from tradingview_ta import TA_Handler, Interval
import time
from datetime import datetime
import config
from binance.client import Client
from binance.enums import * 
#----  
import matplotlib.pyplot as plt
import numpy as np
import talib as ta  
#-------------------------------------
from db_connect import * 
#-------------------------------------
now = datetime.now()
fecha = now.strftime("%d-%m-%y %H:%M:%S")
#-------------------------------------
#client = Client(config.API_KEY, config.API_SECRET, tld='com')
client = Client(config.API_KEY, config.API_SECRET)
messenger = Sendline(config.TOKEN_LINE) 
g_coin_format = "{0:12.5f}"

#=== Exmple Percent & Amount ===
#  amount = 0.000234234
#  precision = 5
#  amt_str = "{:0.0{}f}".format(amount, precision)
#-------------------------------
#=== BUY & SELL (Limit/Market) ===  
# order = client.create_order(symbol='BNBBTC',side=SIDE_BUY,type=ORDER_TYPE_LIMIT,timeInForce=TIME_IN_FORCE_GTC,quantity=100,price='0.00001')
#-------------------------------
#=== BUY(Limit/Market) ===   
#def PlaceBUYLimit(amount,symbol)  # order = client.order_limit_buy(symbol='BNBBTC',quantity=100,price='0.00001') 
#def PlaceBUYMarket(amount,symbol)  # order = client.order_market_buy(symbol='BNBBTC',quantity=100)
#-------------------------------
#=== BUY(Limit/Market) === 
#def PlaceSELLLimit(amount,symbol)  # order = client.order_limit_sell(symbol='BNBBTC',quantity=100,price='0.00001')
#def PlaceSELLMarket(amount,symbol) # order = client.order_market_sell(symbol='BNBBTC',quantity=100)
#-------------------------------
def date_diff_in_Seconds(dt2, dt1):
  timedelta = dt2 - dt1
  return timedelta.days * 24 * 3600 + timedelta.seconds
#-------------------------------
def check_signal_coin(conn,coin,data_analysis,last_prices): 
    #data_cmp = coin +'-'+ data_summary["RECOMMENDATION"] +'-'+ str(data_indi["RSI"]) +'-'+ str(data_indi["SMA5"]) +'-'+ str(data_indi["SMA10"]) +'-'+ str(data_indi["SMA20"] )
    sql_str = sql_last_signal_by_coin.replace("#coin#", coin)  
    data = get_single_record(conn,sql_str) 
    if len(data) > 0: 
        #print("1 coin [{}] , recommen [{}] , gauge_buy [{}] , gauge_sell [{}] ,gauge_neutral [{}]".format(coin ,data_analysis.summary["RECOMMENDATION"] , data_analysis.summary["BUY"] , data_analysis.summary["SELL"] ,data_analysis.summary["NEUTRAL"] ) )
        #print("2 coin [{}] , recommen [{}] , gauge_buy [{}] , gauge_sell [{}] ,gauge_neutral [{}]".format(data[0] , data[1] , data[2] ,data[3] ,data[4]) )
        if data[0] == coin and data[1] == data_analysis.summary["RECOMMENDATION"] and data[2] == data_analysis.summary["BUY"] and data[3] == data_analysis.summary["SELL"] and data[4] == data_analysis.summary["NEUTRAL"] :
            return False
        else :
            return True    
    else :
        return True    
#-------------------------------
def get_tradingview(coin): 
    #print(coin) 
    tesla = TA_Handler()
    conn = get_connection() 
    tesla.set_symbol_as(coin)
    tesla.set_exchange_as_crypto_or_stock("BINANCE")
    tesla.set_screener_as_crypto()
    tesla.set_interval_as(Interval.INTERVAL_1_MINUTE)
    try: 
      return tesla.get_analysis()    
    except Exception as e:
      print("No Data")
      return [] 
      #continue 
#-------------------------------
def get_prices(coin): 
    last_prices = {}
    #print("----------------- get_prices : [{}] -----------------".format(coin))
    #ต้องหาวิธี Get Current Balance ใหม่ 
    try:
        tickers = client.get_ticker(symbol=coin) 
        last_prices['symbol'] = coin
        last_prices['prevClosePrice'] = float(tickers['prevClosePrice'])
        last_prices['openPrice'] = float(tickers['openPrice'])
        last_prices['lastPrice'] = float(tickers['lastPrice'])
        last_prices['askPrice'] = float(tickers['askPrice'])
        last_prices['askQty'] = float(tickers['askQty']) 
        return last_prices 
    except Exception as e:
      print("No Data")
      return [] 
    #print ('ราคาล่าสุด  : ' + tickers['lastPrice']) 
    #print ('ราคาขายถูกสุด  : ' + tickers['askPrice'])
    #print ('จำนวนราคาขายถูกสุด  : ' + tickers['askQty'])
#-------------------------------
def get_prev_prices(coin): 
    last_prices = {}
    #print("----------------- get_prices : [{}] -----------------".format(coin))
    try:
        # Open time,Open,High,Low,Close,Volume,Close time,Quote asset volume,Number of trades,Taker buy base asset volume,Taker buy quote asset volume,Can be ignored
        klines = client.get_historical_klines(coin, Client.KLINE_INTERVAL_1MINUTE, "3 minutes ago UTC")
        #closes = [float(i[4]) for i in klines] # List Comprehension 
        last_prices['openPrice-1']     = float(klines[0][1])
        last_prices['high-1']          = float(klines[0][2])
        last_prices['low-1']           = float(klines[0][3])
        last_prices['closePrice-1']    = float(klines[0][4])
        last_prices['volume-1']        = float(klines[0][5])
        last_prices['NumberOfTrad-1']  = float(klines[0][8]) 

        last_prices['openPrice-2']     = float(klines[1][1])
        last_prices['high-2']          = float(klines[1][2])
        last_prices['low-2']           = float(klines[1][3])
        last_prices['closePrice-2']    = float(klines[1][4])
        last_prices['volume-2']        = float(klines[1][5])
        last_prices['NumberOfTrad-2']  = float(klines[1][8]) 

        last_prices['openPrice-3']     = float(klines[2][1])
        last_prices['high-3']          = float(klines[2][2])
        last_prices['low-3']           = float(klines[2][3])
        last_prices['closePrice-3']    = float(klines[2][4])
        last_prices['volume-3']        = float(klines[2][5])
        last_prices['NumberOfTrad-3']  = float(klines[2][8])        
 
        return last_prices 
    except Exception as e:
      print("No Data")
      return [] 
    #print ('ราคาล่าสุด  : ' + tickers['lastPrice']) 
    #print ('ราคาขายถูกสุด  : ' + tickers['askPrice'])
    #print ('จำนวนราคาขายถูกสุด  : ' + tickers['askQty'])
#-------------------------------
def get_msg_signal_line(coin,last_prices,data_analysis): 
    msg_str = '\n' 
    if data_analysis.summary["RECOMMENDATION"] == "BUY" or  data_analysis.summary["RECOMMENDATION"] == "STRONG_BUY" :
        msg_str = msg_str + '-----------------------------------------------------------\n'
        msg_str = msg_str + ' \U0001F4B2  \U0001F49A\U0001F49A\U0001F49A  '+ coin +'  \U0001F49A\U0001F49A\U0001F49A \U0001F4B2  \n'
        msg_str = msg_str + '-----------------------------------------------------------\n'
        msg_str = msg_str + '       \U0001F3AF  จุดเข้า   99.99999    \U0001F3AF \n'
        msg_str = msg_str + '-----------------------------------------------------------\n'
    elif data_analysis.summary["RECOMMENDATION"] == "SELL" or  data_analysis.summary["RECOMMENDATION"] == "STRONG_SELL" :
        msg_str = msg_str + '-----------------------------------------------------------\n' 
        msg_str = msg_str + ' \U0001F4B2  \U00002764\U00002764\U00002764  '+ coin +'  \U00002764\U00002764\U00002764 \U0001F4B2  \n'
        msg_str = msg_str + '-----------------------------------------------------------\n' 
        msg_str = msg_str + '       \U0001F3AF  จุดออก  99.99999    \U0001F3AF \n'
        msg_str = msg_str + '-----------------------------------------------------------\n'
    msg_str = msg_str + ' ราคาล่าสุด   : '+ str(last_prices['lastPrice']) + ' \n'
    msg_str = msg_str + ' SRI              : '+ str(data_analysis.indicators["RSI"]) + ' \n'
    msg_str = msg_str + ' SMA5         : '+ str(data_analysis.indicators["SMA5"]) + ' \n'
    msg_str = msg_str + ' SMA10       : '+ str(data_analysis.indicators["SMA10"]) + ' \n'
    msg_str = msg_str + ' SMA20       : '+ str(data_analysis.indicators["SMA20"]) + ' \n'
    msg_str = msg_str + ' SMA30       : '+ str(data_analysis.indicators["SMA30"]) + ' \n'
    msg_str = msg_str + ' SMA100     : '+ str(data_analysis.indicators["SMA100"]) + ' \n'
    msg_str = msg_str + '-----------------------------------------------------------\n'
    return msg_str
#-------------------------------
def inst_signal(conn,coin,last_prices,data_analysis):   
    rec = []
    data = [] 
    #print("----------------- inst_signal : [{}] -----------------".format(coin)) 
    rec.append(coin)
    rec.append(data_analysis.summary["RECOMMENDATION"])
    rec.append(data_analysis.summary["BUY"])
    rec.append(data_analysis.summary["SELL"])
    rec.append(data_analysis.summary["NEUTRAL"])
    rec.append(last_prices['lastPrice']) 
    rec.append(data_analysis.indicators["RSI"])
    rec.append(data_analysis.indicators["MACD.macd"])
    rec.append(data_analysis.indicators["MACD.signal"])
    rec.append(data_analysis.indicators["ADX"])
    rec.append(data_analysis.indicators["ADX+DI"])
    rec.append(data_analysis.indicators["ADX-DI"])
    rec.append(data_analysis.indicators["ADX+DI[1]"])
    rec.append(data_analysis.indicators["ADX-DI[1]"])
    rec.append(data_analysis.indicators["SMA5"])
    rec.append(data_analysis.indicators["SMA10"])
    rec.append(data_analysis.indicators["SMA20"])
    rec.append(data_analysis.indicators["SMA30"])
    rec.append(data_analysis.indicators["SMA50"])
    rec.append(data_analysis.indicators["SMA100"])
    rec.append(data_analysis.indicators["SMA200"])
    rec.append(data_analysis.indicators["EMA5"])
    rec.append(data_analysis.indicators["EMA10"])
    rec.append(data_analysis.indicators["EMA20"])
    rec.append(data_analysis.indicators["EMA30"])
    rec.append(data_analysis.indicators["EMA50"])
    rec.append(data_analysis.indicators["EMA100"])
    rec.append(data_analysis.indicators["EMA200"])
    rec.append(data_analysis.indicators["Pivot.M.Fibonacci.S3"])
    rec.append(data_analysis.indicators["Pivot.M.Fibonacci.S2"])
    rec.append(data_analysis.indicators["Pivot.M.Fibonacci.S1"])        
    rec.append(data_analysis.indicators["Pivot.M.Fibonacci.Middle"])
    rec.append(data_analysis.indicators["Pivot.M.Fibonacci.R1"])
    rec.append(data_analysis.indicators["Pivot.M.Fibonacci.R2"])  
    rec.append(data_analysis.indicators["Pivot.M.Fibonacci.R3"])        
    rec.append("ACTIVE")
    rec.append(str(data_analysis.time)[0:19]) 
    data.append(rec)
    #print(data)
    insert_data(conn,sql_inst_signals,data)
#-------------------------------
def inst_trades(conn,coin,buy_price,buy_unit,formula_buy,percent_cut_wins,percent_cut_loss): 
    rec = []
    data = [] 
    print("----------------- inst_trades : [{}] -----------------".format(coin))
    rec.append(coin)
    rec.append("SPORT") 
    rec.append(buy_unit) 
    rec.append(buy_price)  
    rec.append(formula_buy)       #formula_buy 
    rec.append(percent_cut_wins)  #percent_cut_wins 
    rec.append(percent_cut_loss)  #percent_cut_loss 
    rec.append("WAIT_SELL") 
    data.append(rec)
    #print(data)
    insert_data(conn,sql_inst_trades,data)    
#-------------------------------
def upd_trades(conn,coin,sell_price,formula_sell):  
    print("----------------- upd_trades : [{}] -----------------".format(coin))  
    sql_str =  sql_upd_trades.replace("#coin#", coin).replace("#sell_price#", str(sell_price)).replace("#market_type#", "SPORT").replace("#status#", "WAIT_SELL").replace("#formula_sell#", formula_sell)      
    update_data(conn,sql_str)    
#-------------------------------
def send_line(line_info): 
    #line_info['mode'] #  msg , msg_sticker ,image
    #line_info['message']
    #line_info['sticker_id']
    #line_info['package_id']
    #line_info['img_url']
    if (line_info['mode'] == 'msg'):
        messenger.sendtext(line_info['message']) 
    elif(line_info['mode'] == 'msg_sticker'): 
        messenger.sticker(line_info['sticker_id'],line_info['package_id'],line_info['message'])  
    elif(line_info['mode'] == 'image'):    
        messenger.sendimage(line_info['img_url'])
#-------------------------------
def lst_my_portfolio(conn , coin = 'ALL'): 
    result = []
    #print("----------------- lst_my_portfolio For Coin : [{}] -----------------".format(coin))
    sql_str = sql_my_portfolio.replace("#coin#", coin).replace("#status#", "WAIT_SELL")
    data = get_multiple_record(conn,sql_str) 
    if len(data) > 0: 
        for index in range(len(data)):
            result.append(data[index])  
    return result
    #INSERT INTO trades (coin_code , market_type ,unit ,buy_price ,sell_price ,trade_dtm  ,status ,status_dtm )VALUES
    #('BCTUSDT','SPORT',10,0.00156,null,datetime('now', 'localtime'),'WAIT_SELL',datetime('now', 'localtime'))        
#-------------------------------
def get_formula_by_coin(conn , coin , formula_type) : 
    result = []
    #print("----------------- get_formula_by_coin For Coin : [{}] -----------------".format(coin))
    if formula_type == 'BUY' :
        sql_str = sql_formula_buy_trade.replace("#coin#", coin)
    elif formula_type == 'SELL' :
        sql_str = sql_formula_sell_trade.replace("#coin#", coin) 
    #print(sql_str)    
    data = get_single_record(conn,sql_str) 
    if len(data) > 0:   
        result = data
    return result
    #INSERT INTO "formulas"("formula_code", "formula_type", "rsi", "macd", "wins_cnt", "loss_cnt", "status", "status_dtm")
    #VALUES('B001', 'BUY', 40, 0, 0, 0, 'ACTIVE', datetime('now', 'localtime'))
  
    #INSERT INTO "formulas"("formula_code", "formula_type", "rsi", "macd", "wins_cnt", "loss_cnt", "status", "status_dtm")
    #VALUES('S001', 'SELL', 70, 0, 0, 0, 'ACTIVE', datetime('now', 'localtime'))         
#-------------------------------
#================================================================================== 
def ACTION_BUY(conn,coin,formula_buy,percent_cut_wins,percent_cut_loss,price_cut_wins,price_cut_loss):            
    line_info = {} 
    #print("----------------- ACTION_BUY : [{}]  -----------------".format(coin))
    #Buy Coin In Binance
    #Get Port by Coin
    last_prices = get_prices(coin)
    buy_price  = last_prices['lastPrice']
    buy_unit   = 10
    inst_trades(conn,coin,buy_price,buy_unit,formula_buy,percent_cut_wins,percent_cut_loss)
    print("### BUY [{}] ,Price : {} , Price Wins : {} , Price Loss : {}  ".format(coin,buy_price,price_cut_wins,price_cut_loss))
    line_info['mode']       = 'msg'  #  msg , msg_sticker ,image
    line_info['message']    =  '\n  BUY [' + coin  + '] \n Price : ' + str(buy_price) + '\n  Wins['+  percent_cut_wins +'%] : ' + str(price_cut_wins) + '\n Loss[' + percent_cut_loss +'%] : ' + str(price_cut_loss)
    send_line(line_info) 
#-------------------------------  
def CHECK_SIGNAL_FOR_BUY(conn,coin,last_prices,data_analysis):
    action_buy   = "BUY"   # For Test 
    my_coin = []
    buy_formula = []         
    #trade_growth =
    signal_buy      = False  
    #print("----------------- CHECK SIGNAL COIN FOR BUY : [{}] -----------------".format(coin))
    #Check Conin & %Price & Trade Dtm In Portfolio
    my_coin_portfolio = lst_my_portfolio(conn,coin) 
    #get formula
    buy_formula = get_formula_by_coin(conn,coin,'BUY')
    if len(buy_formula) > 0 :
        limit_coin        = buy_formula[1] 
        formula_buy       = buy_formula[2] 
        formula_sell      = buy_formula[3] 
        percent_cut_wins  = buy_formula[4] 
        percent_cut_loss  = buy_formula[5]
        rsi_buy           = buy_formula[6]
        macd_buy          = buy_formula[7]
        #print("**** FORMULAR BUY [{}] ,Limit : {} , Formular Buy : {} , Formular Sell : {} , % Wins : {} , % Loss : {}, RSI : {} , MACD : {}  ".format(coin,limit_coin ,formula_buy,formula_sell,percent_cut_wins,percent_cut_loss,rsi_buy,macd_buy))

        if len(my_coin_portfolio) > 0 :
            if len(my_coin_portfolio) >= limit_coin : 
                signal_buy = False
            else :    
                coin        =  my_coin_portfolio[0][0]
                buy_unit    =  my_coin_portfolio[0][2]
                buy_price   =  my_coin_portfolio[0][3]
                trade_time  =  my_coin_portfolio[0][5]
                signal_buy  = True
        else :
            signal_buy  = True
        #Check Conin Up Trande & %  
        if signal_buy :
            rsi              = data_analysis.indicators["RSI"]
            macd             = data_analysis.indicators["MACD.macd"]
            adx              = data_analysis.indicators["ADX"]
            open_price       = last_prices['openPrice']
            last_price       = last_prices['lastPrice']
            close_price_prev = last_prices['prevClosePrice']
            ema_fast         = data_analysis.indicators["EMA5"]
            ema_slow         = data_analysis.indicators["EMA50"] 
            ema_20           = data_analysis.indicators["EMA20"]
            adx_plus         = data_analysis.indicators["ADX+DI"]
            adx_div          = data_analysis.indicators["ADX-DI"]
            #close_price_prev = data_analysis.indicators["close"] 
            price_cut_wins   = last_price - (percent_cut_wins/100) * last_price
            price_cut_loss   = last_price - (percent_cut_loss/100) * last_price  
            #print("SIGNAL BUY:[{}], Last[{}], OpenPrice[{}], ClosePrice[{}], RSI[{}], MACD[{}], EMA Fast[{}], EMA Slow[{}], EMA20[{}]".format(coin,g_coin_format.format(last_price),g_coin_format.format(open_price),g_coin_format.format(close_price_prev),g_coin_format.format(rsi),g_coin_format.format(macd),g_coin_format.format(ema_fast),g_coin_format.format(ema_slow),g_coin_format.format(ema_20)))

            #if True :
            #if ema_fast > ema_slow and rsi <= rsi_buy and macd > 0 and last_price > ema_slow and adx_plus > adx_div : #adx > ema_20 and
            #if rsi <= rsi_buy  and ema_fast > ema_slow and macd > 0 and last_price > ema_slow : #adx > ema_20 and
            if formula_buy == 'B001' :
                print("Formular BUY[{},{}] : if rsi_buy[{}] >= rsi[{}] and macd[{}] > macd_buy[{}] and open_price[{}] > close_price_prev[{}] and last_price[{}] > ema_fast[{}] ".format(coin,formula_buy,g_coin_format.format(rsi_buy),g_coin_format.format(rsi),g_coin_format.format(macd),macd_buy,g_coin_format.format(open_price),g_coin_format.format(close_price_prev),g_coin_format.format(last_price),g_coin_format.format(ema_fast) ))
                if rsi_buy >= rsi and macd > macd_buy and open_price > close_price_prev and last_price > ema_fast :     
                    signal_buy = True 
                else :
                    signal_buy = False
            else :
                signal_buy = False

        if signal_buy :
            ACTION_BUY(conn,coin,formula_buy,percent_cut_wins,percent_cut_loss,price_cut_wins,price_cut_loss)   
    else :
        print("**** Not Foud Formular BUY By Coin {}  ".format(coin))
#-------------------------------     
def ACTION_SELL(conn,portfolio,formula_sell,sell_type):
    line_info = {} 
    coin        =  portfolio[0] 
    #Sell Coin In Binance
    #Get Port by Coin
    last_prices = get_prices(coin)
    sell_price  = last_prices['lastPrice']    
    #---------------------------------------       
    print("----------------- ACTION_SELL : [{}] -----------------".format(coin)) 
    buy_price   =  portfolio[3]
    upd_trades(conn,coin,sell_price,formula_sell)
    print("**** SELL-{} [{}] ,Price : {} ".format(sell_type,coin,sell_price))
    line_info['mode']       = 'msg'  #  msg , msg_sticker ,image
    line_info['message']    =  '\n  SELL(' + sell_type +') [' + coin  + '] \n BUY Price : ' + str(buy_price) + '\n  SELL Price : ' + str(sell_price)
    send_line(line_info) 
#-------------------------------  
def CHECK_SIGNAL_FOR_SELL(conn,portfolio):
    action_sell  = "SELL"   # For Test 
    sell_formula = []   
    coin            =  portfolio[0]
    buy_unit        =  portfolio[2]
    buy_price       =  portfolio[3]
    trade_time      =  portfolio[5]      
    #-- Get Data From API  
    data_analysis    = get_tradingview(coin)
    last_prices      = get_prices(coin)
    sell_formula     = get_formula_by_coin(conn,coin,'SELL')
    if len(sell_formula) > 0 :
        limit_coin        = sell_formula[1] 
        formula_buy       = sell_formula[2] 
        formula_sell      = sell_formula[3] 
        percent_cut_wins  = sell_formula[4] 
        percent_cut_loss  = sell_formula[5]
        rsi_buy           = sell_formula[6]
        macd_buy          = sell_formula[7]
        #print("**** FORMULAR SELL [{}] ,Limit : {} , Formular Buy : {} , Formular Sell : {} , % Wins : {} , % Loss : {}, RSI : {} , MACD : {}  ".format(coin,limit_coin ,formula_buy,formula_sell,percent_cut_wins,percent_cut_loss,rsi_buy,macd_buy))
         
        rsi              = data_analysis.indicators["RSI"]
        macd             = data_analysis.indicators["MACD.macd"]
        adx              = data_analysis.indicators["ADX"]
        open_price       = last_prices['openPrice']
        last_price       = last_prices['lastPrice']
        close_price_prev = last_prices['prevClosePrice']
        ema_fast         = data_analysis.indicators["EMA5"]
        ema_slow         = data_analysis.indicators["EMA50"] 
        ema_20           = data_analysis.indicators["EMA20"]
        adx_plus         = data_analysis.indicators["ADX+DI"]
        adx_div          = data_analysis.indicators["ADX-DI"]
        #close_price_prev = data_analysis.indicators["close"] 
        price_cut_wins   = buy_price - (percent_cut_wins/100) * buy_price
        price_cut_loss   = buy_price - (percent_cut_loss/100) * buy_price  
        #---------------------------------------     
        #print("----------------- CHECK SIGNAL COIN FOR SELL : [{} , {}] BUY Price {} , BUY Unit {} ,Trade Time {} , Price Cut WINS {}   , Price Cut LOSS {}   -----------------".format(coin,last_price , buy_price , buy_unit , trade_time , price_cut_win, price_cut_loss))
        #print("----------------- CHECK SIGNAL COIN FOR SELL : [{} , L[{}]  , B[{}] ] , Diff Price :{} , Diff % : {}  -----------------".format(coin , last_price , buy_price, last_price - buy_price ,  ((last_price - buy_price)/last_price)*100 ))
        #print( "SIGNAL SELL:[{}], Last[{}], Buy[{}], Diff[{}], Diff%[{}], WINS[{}], LOSS[{}]".format( coin , g_coin_format.format(last_price) , g_coin_format.format(buy_price)  , g_coin_format.format(last_price - buy_price ) , g_coin_format.format(((last_price - buy_price)/last_price)*100) , g_coin_format.format(price_cut_wins) , g_coin_format.format(price_cut_loss) ) )  
        if formula_sell == 'S001' :
            print("Formular BUY[{},{}] : if rsi_buy[{}] <= rsi[{}] and macd[{}] < macd_buy[{}] and open_price[{}] < close_price_prev[{}] and last_price[{}] < ema_fast[{}] ".format(coin,formula_sell,g_coin_format.format(rsi_buy),g_coin_format.format(rsi),g_coin_format.format(macd),macd_buy,g_coin_format.format(open_price),g_coin_format.format(close_price_prev),g_coin_format.format(last_price),g_coin_format.format(ema_fast) ))
            if rsi_buy <= rsi and macd < macd_buy and open_price < close_price_prev and last_price < ema_fast :
                if last_price > price_cut_wins :
                    ACTION_SELL(conn,portfolio,formula_sell,'WINS')
                elif last_price < price_cut_loss :
                    ACTION_SELL(conn,portfolio,formula_sell,'LOSS')   
    else :
        print("**** Not Foud Formular SELL By Coin {}  ".format(coin))        
#-------------------------------     



#=== Check Signal By coin ===
def TRADE_SPORT_CURRENT():
    #print("----------------- Start TRADE_SPORT_CURRENT -----------------")
    my_portfolios = []  # Coins On Portfolio
    #--------------------------------------
    #-- Get Data From API
    conn          = get_connection()  
    #data_analysis = get_tradingview(coin)
    #last_prices   = get_prices(coin) 
    #---------------------------------------    
    my_portfolios = lst_my_portfolio(conn) 
    for portfolio in my_portfolios : 
        CHECK_SIGNAL_FOR_SELL(conn,portfolio)
#-------------------------------  
def SIGNALS_BY_SYMBOL_TDV(coin):    
    line_info = {}
    data_analysis = [] 
    #print("----------------- Start SIGNALS_BY_SYMBOL_TDV : [{}] -----------------".format(my_coin))
    #--------------------------------------
    #-- Get Data From API
    conn          = get_connection()  
    data_analysis = get_tradingview(coin)
    last_prices   = get_prices(coin) 
    #---------------------------------------
   
    if  check_signal_coin(conn,coin,data_analysis,last_prices) :
        #print("{}  [{}:{}] , RSI [{}] , Close [{}] , RSI1 [{}] , SMA5 [{}] , SMA10 [{}] , SMA20 [{}] , SMA30 [{}] , SMA100 [{}] ".format(data_analysis.summary["RECOMMENDATION"],coin ,last_prices['lastPrice'] ,data_analysis.indicators["RSI"] ,data_analysis.indicators["close"],data_analysis.indicators["RSI[1]"],data_analysis.indicators["SMA5"],data_analysis.indicators["SMA10"],data_analysis.indicators["SMA20"],data_analysis.indicators["SMA50"],data_analysis.indicators["SMA100"]))     
        if( data_analysis.summary["RECOMMENDATION"])=="BUY":
            inst_signal(conn,coin,last_prices,data_analysis)
            CHECK_SIGNAL_FOR_BUY(conn,coin,last_prices,data_analysis)
            #line_info['mode']       = 'msg'  #  msg , msg_sticker ,image
            #line_info['message']    =  get_msg_signal_line(coin,last_prices,data_analysis)
            #send_line(line_info)   

        elif( data_analysis.summary["RECOMMENDATION"])=="SELL":
            inst_signal(conn,coin,last_prices,data_analysis) 
            CHECK_SIGNAL_FOR_BUY(conn,coin,last_prices,data_analysis)
            #line_info['mode']       = 'msg'  #  msg , msg_sticker ,image
            #line_info['message']    =  get_msg_signal_line(coin,last_prices,data_analysis)
            #send_line(line_info)   

        elif( data_analysis.summary["RECOMMENDATION"])=="STRONG_BUY": 
            inst_signal(conn,coin,last_prices,data_analysis)
            CHECK_SIGNAL_FOR_BUY(conn,coin,last_prices,data_analysis)
            line_info['mode']       = 'msg'  #  msg , msg_sticker ,image
            line_info['message']    =  get_msg_signal_line(coin,last_prices,data_analysis)
            send_line(line_info)            

        elif( data_analysis.summary["RECOMMENDATION"])=="STRONG_SELL": 
            inst_signal(conn,coin,last_prices,data_analysis) 
            CHECK_SIGNAL_FOR_BUY(conn,coin,last_prices,data_analysis)
            line_info['mode']       = 'msg'  #  msg , msg_sticker ,image
            line_info['message']    =  get_msg_signal_line(coin,last_prices,data_analysis)
            send_line(line_info)   
    #else :
    #    print('.')
    #print("----------------- End SIGNALS_BY_SYMBOL_TDV : [{}] -----------------".format(coin))         
#------------------------------- 
def SIGNALS_BY_SYMBOL(coin):
    print("----------------- Start SIGNALS_BY_SYMBOL : [{}] -----------------".format(coin))
    # Open time,Open,High,Low,Close,Volume,Close time,Quote asset volume,Number of trades,Taker buy base asset volume,Taker buy quote asset volume,Can be ignored
    klines = client.get_historical_klines(coin, Client.KLINE_INTERVAL_1MINUTE, "30 minutes ago UTC")
    closes = [float(i[4]) for i in klines] # List Comprehension 
    #print("klines  : [{}] ".format(klines)) 
    closes = np.array(closes)
    #print(closes)

    ema12 = ta.EMA(closes,timeperiod=5)
    ema26 = ta.EMA(closes,timeperiod=10)
    rsi   = ta.RSI(closes,timeperiod=12) 
    #print("rsi  : [{}] ".format(rsi))
    #figure Draw graph
    #fig = plt.figure()
    #axes = fig.add_axes([0.1,0.1,0.8,0.8])
    #axes.set_xlabel("1min time frame")
    #axes.set_ylabel("PRICE {}". format(coin))

    #plot graph
    #plt.plot(closes,"--",color="gray",label="Price")
    #plt.plot(ema12,"-",color="green",label="ema12")
    #plt.plot(ema26,"-",color="red",label="ema24")
    #plt.plot(rsi,"-",color="yellow",label="rsi")

    #cross over / cross under
    crossover  = [] #buy
    crossunder = [] #sell

    for index,val in enumerate(zip(ema12,ema26)) :
        i = val[0]
        j = val[1]
        #print(i , ' , ' , j)
        #cross over  
        if (ema12[index-1] < ema26[index-1]) and (i > j): 
            print("BULLISH Hear FOR {} : {} , {}".format(coin,ema12[index-1]-ema26[index-1],i-j))  
            #BUYH
            #Cal Amount 
            #PlaceBUY
            crossover.append(i)  #จุดที่มีการ cross
            break
        elif (ema12[index-1] > ema26[index-1]) and (i < j):
            print("BEARISH Hear FOR {} : {} , {}".format(coin,ema12[index-1]-ema26[index-1],i-j))              
            #SELL
            #PlaceSELL
            crossunder.append(i) #จุดที่มีการ cross
            break
        else:
             #print("NO SIGNAL FOR {}".format(coin))
             crossover.append(None)  #จุดที่มีการ cross
             crossunder.append(None) #จุดที่มีการ cross
             
            
    crossover = np.array(crossover)
    crossunder = np.array(crossunder)
    
    #if  crossover.size >= 1 :   
    #    print("crossover  : [{}] ".format(crossover))
    #if  crossunder.size >= 1 :       
    #    print("crossunder  : [{}] ".format(crossunder))
    #print(crossover)
    #print(crossunder)

    #plt.plot(crossover,"x",color="green",label="BULLISH")
    #plt.plot(crossunder,"x",color="red",label="BEARISH")

    #plt.legend(loc="upper left")
    #plt.show()
#-------------------------------  


